module Color
  extend self

  macro add_color(name, *codes)
    def {{name}}(str)
      wrap(str, {{codes}})
    end
  end

  private def wrap(str, codes)
    return str unless STDOUT.tty?
    "\e[#{codes.join(';')}m#{str}\e[0m"
  end

  add_color bold, 1
  add_color yellow, 33
  add_color cyan, 36
end
