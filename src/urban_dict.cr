require "myhtml"
require "http/client"
require "./*"

input = ARGV.join(' ')
case input
when .empty?
  abort "Usage: ud <word> [<word>...]"
when "-h", "--help"
  abort "Usage: ud <word> [<word>...]", 0
end

formatted_input = input.gsub(' ', "%20")
url = "https://www.urbandictionary.com/define.php?term=#{formatted_input}"

def wrap_long_string(str, width = 80)
  str.gsub(/(.{1,#{width}})(\s+|\Z)/, "\t\\1\n")
end

def html(url, redirects = 4) : String
  abort "Too many redirects" if redirects == 0

  client = HTTP::Client.get url

  if {301, 302}.includes? client.status_code
    new_location = client.headers["location"]
    html(new_location, redirects - 1)
  else
    client.body
  end
end

doc = Myhtml::Parser.new(html(url))

title = doc.css("title").first

ribbons = doc.css("div.ribbon")
meanings = doc.css("div.meaning")
examples = doc.css("div.example")
[ribbons, meanings, examples].any?(&.empty?) && abort "Nothing found for: #{input}"

printf "\n==[%s]==\n\n", Color.bold(title.inner_text.lchop("Urban Dictionary: "))
ribbons.zip(meanings, examples).each do |ribbon, meaning, example|
  next if ribbon.inner_text.includes?("Word of the Day")
  printf(
    "⚫⚫⚫\n%s\n%s",
    Color.cyan(wrap_long_string(meaning.inner_text.chomp)),
    Color.yellow(wrap_long_string(example.inner_text.chomp))
  )
end
